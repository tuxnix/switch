### switch
Switch ist ein Skript zur Steuerung des SwitchPlugins von TV-Browser.  
Mit switch kann zwischen den Sendern gezapped werden.
#### 1. Installation
Benötigt wird die Installation des Paketes `mpv`
Das Skript wird als Datei `/usr/local/bin/switch` gespeichert und anschließend mit `chmod +x /usr/local/bin/switch` ausführbar gemacht.
#### 2. Konfiguration
Zuerst muss das SwitchPlugin im TV-Browser im Menu Extras -> Pluins verwalten geladen werden.  
Dort wird dann als "zu startendes externes Programm" `/usr/local/bin/switch` und bei 
Parameter `{channel_name}` eingetragen.
#### 3. Benutzung
Die Benutzung ist recht einfach. Mit der rechten Maustaste wählt man den Sender aus. Es erschein ein Untermenue. Dort wählt man mit der rechten Maustaste "Switch" aus.
#### 4. Weiters
Einige Sender URLs der öffentlich rechtlichen Sender in der BDR sind im Skript bereis gelistet (Stand Juni 2023). Man kann beliebig viele andere Sender hinzufügen. Die Namen der Sender sollten dann im TV-Browser und im Skript jeweils angepasst werden.
